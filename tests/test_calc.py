import pytest

from calc.calc import Calc

# group with similar features, could test with each units
def test_add_or_sub():
    c = Calc()

# def test_add_two_numbers():
    assert c.add(4, 5) == 9

# def test_add_three_numbers():
    assert c.add(4, 5, -6) == 3

# def test_add_many_numbers():
    assert c.add(*range(100)) == 4950

# def test_subtract_two_numbers():
    assert c.sub(10, 3) == 7

def test_mul_or_div():
    c = Calc()

# def test_mul_two_numbers():
    assert c.mul(6, 4) == 24

# def test_mul_many_numbers():
    assert c.mul(*range(1, 10)) == 362880

# def test_div_two_numbers_float():
    assert c.div(13, 2) == 6.5

# def test_div_by_zero_returns_inf():
    assert c.div(5, 0) == "inf"

# def test_mul_by_zero_raises_exception():
    with pytest.raises(ValueError):
        c.mul(3, 0)

def test_agv():
    c = Calc()

# def test_avg_correct_average():
    assert c.avg([2, 5, 12, 98]) == 29.25

# def test_avg_removes_upper_outliers():
    assert c.avg([2, 5, 12, 98], ut=90) == pytest.approx(6.333333)

# def test_avg_removes_lower_outliers():
    assert c.avg([2, 5, 12, 98], lt=10) == pytest.approx(55)

# def test_avg_upper_threshhold_is_included():
    assert c.avg([2,5,12,98], ut=98) == 29.25

# def test_avg_lower_threshhold_is_included():
    assert c.avg([2,5,12,98], lt=2) == 29.25

# def test_avg_empty_list():
    assert c.avg([]) == 0

# def test_avg_manages_empty_list_after_outlier_removal():
    assert c.avg([12,98], lt=15, ut=90) == 0

# def test_avg_manages_empty_list_before_outlier_removal():
    assert c.avg([], lt=15, ut=90) == 0

# def test_avg_manages_zero_value_lower_outlier():
    assert c.avg([-1,0,1], lt=0) == 0.5

# def test_avg_manages_zero_value_upper_outlier():
    assert c.avg([-1,0,1], ut=0) == -0.5
